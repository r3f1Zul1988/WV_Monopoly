﻿namespace Monopoly
{
    partial class Gameboard
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.spielToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dice1 = new System.Windows.Forms.Label();
            this.dice2 = new System.Windows.Forms.Label();
            this.btn_roll = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.player8 = new Monopoly.Model.Player();
            this.player7 = new Monopoly.Model.Player();
            this.player6 = new Monopoly.Model.Player();
            this.player5 = new Monopoly.Model.Player();
            this.player4 = new Monopoly.Model.Player();
            this.player3 = new Monopoly.Model.Player();
            this.player2 = new Monopoly.Model.Player();
            this.player1 = new Monopoly.Model.Player();
            this.schlossstreet = new Monopoly.Model.Street();
            this.addtax = new Monopoly.Model.Street();
            this.berlinerstreet = new Monopoly.Model.Street();
            this.wienerstreet = new Monopoly.Model.Street();
            this.parkstreet = new Monopoly.Model.Street();
            this.event3 = new Monopoly.Model.Street();
            this.hauptbhf = new Monopoly.Model.Street();
            this.bahnhofstreet = new Monopoly.Model.Street();
            this.community3 = new Monopoly.Model.Street();
            this.hauptstreet = new Monopoly.Model.Street();
            this.rathausstreet = new Monopoly.Model.Street();
            this.community2 = new Monopoly.Model.Street();
            this.muenchenerstreet = new Monopoly.Model.Street();
            this.westbhf = new Monopoly.Model.Street();
            this.neuestreet = new Monopoly.Model.Street();
            this.harborstreet = new Monopoly.Model.Street();
            this.electricity = new Monopoly.Model.Street();
            this.seestreet = new Monopoly.Model.Street();
            this.goethestreet = new Monopoly.Model.Street();
            this.water = new Monopoly.Model.Street();
            this.schillerstreet = new Monopoly.Model.Street();
            this.lessingstreet = new Monopoly.Model.Street();
            this.northbhf = new Monopoly.Model.Street();
            this.opernstreet = new Monopoly.Model.Street();
            this.museumstreet = new Monopoly.Model.Street();
            this.event2 = new Monopoly.Model.Street();
            this.theaterstreet = new Monopoly.Model.Street();
            this.gotojail = new Monopoly.Model.Street();
            this.freepark = new Monopoly.Model.Street();
            this.jail = new Monopoly.Model.Street();
            this.poststreet = new Monopoly.Model.Street();
            this.elisenstreet = new Monopoly.Model.Street();
            this.event1 = new Monopoly.Model.Street();
            this.chausseestreet = new Monopoly.Model.Street();
            this.southbhf = new Monopoly.Model.Street();
            this.incometax = new Monopoly.Model.Street();
            this.turmstreet = new Monopoly.Model.Street();
            this.community1 = new Monopoly.Model.Street();
            this.badstreet = new Monopoly.Model.Street();
            this.start = new Monopoly.Model.Street();
            this.streetname = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(143, 284);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Spieler 1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(143, 298);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "Spieler 2:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(243, 285);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Spieler 3:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(243, 298);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "Spieler 4:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(343, 285);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Spieler 5:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(343, 298);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Spieler 6:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(454, 285);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Spieler 7:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(454, 298);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Spieler 8:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(200, 285);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 44;
            this.label9.Text = "1500€";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(200, 298);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "1500€";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(300, 284);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 46;
            this.label11.Text = "1500€";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(300, 298);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 47;
            this.label12.Text = "1500€";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(400, 285);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 13);
            this.label13.TabIndex = 48;
            this.label13.Text = "1500€";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(400, 298);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 49;
            this.label14.Text = "1500€";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(511, 284);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 13);
            this.label15.TabIndex = 50;
            this.label15.Text = "1500€";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(511, 298);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 51;
            this.label16.Text = "1500€";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spielToolStripMenuItem,
            this.optionenToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(729, 24);
            this.menuStrip1.TabIndex = 52;
            this.menuStrip1.Text = "Gamemenu";
            // 
            // spielToolStripMenuItem
            // 
            this.spielToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startenToolStripMenuItem,
            this.beendenToolStripMenuItem});
            this.spielToolStripMenuItem.Name = "spielToolStripMenuItem";
            this.spielToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.spielToolStripMenuItem.Text = "Spiel";
            // 
            // startenToolStripMenuItem
            // 
            this.startenToolStripMenuItem.Name = "startenToolStripMenuItem";
            this.startenToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.startenToolStripMenuItem.Text = "Starten";
            this.startenToolStripMenuItem.Click += new System.EventHandler(this.startenToolStripMenuItem_Click);
            // 
            // beendenToolStripMenuItem
            // 
            this.beendenToolStripMenuItem.Name = "beendenToolStripMenuItem";
            this.beendenToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.beendenToolStripMenuItem.Text = "Beenden";
            this.beendenToolStripMenuItem.Click += new System.EventHandler(this.beendenToolStripMenuItem_Click);
            // 
            // optionenToolStripMenuItem
            // 
            this.optionenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionenToolStripMenuItem1});
            this.optionenToolStripMenuItem.Name = "optionenToolStripMenuItem";
            this.optionenToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.optionenToolStripMenuItem.Text = "Optionen";
            // 
            // optionenToolStripMenuItem1
            // 
            this.optionenToolStripMenuItem1.Name = "optionenToolStripMenuItem1";
            this.optionenToolStripMenuItem1.Size = new System.Drawing.Size(118, 22);
            this.optionenToolStripMenuItem1.Text = "Optionen";
            // 
            // dice1
            // 
            this.dice1.AutoSize = true;
            this.dice1.Location = new System.Drawing.Point(125, 392);
            this.dice1.Name = "dice1";
            this.dice1.Size = new System.Drawing.Size(13, 13);
            this.dice1.TabIndex = 57;
            this.dice1.Text = "6";
            // 
            // dice2
            // 
            this.dice2.AutoSize = true;
            this.dice2.Location = new System.Drawing.Point(190, 392);
            this.dice2.Name = "dice2";
            this.dice2.Size = new System.Drawing.Size(13, 13);
            this.dice2.TabIndex = 58;
            this.dice2.Text = "6";
            // 
            // btn_roll
            // 
            this.btn_roll.Enabled = false;
            this.btn_roll.Location = new System.Drawing.Point(128, 366);
            this.btn_roll.Name = "btn_roll";
            this.btn_roll.Size = new System.Drawing.Size(75, 23);
            this.btn_roll.TabIndex = 59;
            this.btn_roll.Text = "Würfeln";
            this.btn_roll.UseVisualStyleBackColor = true;
            this.btn_roll.Click += new System.EventHandler(this.btn_roll_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(376, 392);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(129, 13);
            this.label17.TabIndex = 70;
            this.label17.Text = "Keine Strasse ausgewählt";
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(379, 366);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 71;
            this.button1.Text = "Haus bauen";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(459, 366);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 72;
            this.button2.Text = "Hotel bauen";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Monopoly.Properties.Resources.Monopoly;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(119, 165);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(493, 104);
            this.pictureBox1.TabIndex = 69;
            this.pictureBox1.TabStop = false;
            // 
            // player8
            // 
            this.player8.BackColor = System.Drawing.Color.Fuchsia;
            this.player8.Location = new System.Drawing.Point(569, 626);
            this.player8.Name = "player8";
            this.player8.Size = new System.Drawing.Size(23, 22);
            this.player8.Street = null;
            this.player8.TabIndex = 68;
            this.player8.Visible = false;
            // 
            // player7
            // 
            this.player7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.player7.Location = new System.Drawing.Point(540, 626);
            this.player7.Name = "player7";
            this.player7.Size = new System.Drawing.Size(23, 22);
            this.player7.Street = null;
            this.player7.TabIndex = 66;
            this.player7.Visible = false;
            // 
            // player6
            // 
            this.player6.BackColor = System.Drawing.Color.White;
            this.player6.Location = new System.Drawing.Point(511, 626);
            this.player6.Name = "player6";
            this.player6.Size = new System.Drawing.Size(23, 22);
            this.player6.Street = null;
            this.player6.TabIndex = 65;
            this.player6.Visible = false;
            // 
            // player5
            // 
            this.player5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.player5.Location = new System.Drawing.Point(482, 626);
            this.player5.Name = "player5";
            this.player5.Size = new System.Drawing.Size(23, 22);
            this.player5.Street = null;
            this.player5.TabIndex = 64;
            this.player5.Visible = false;
            // 
            // player4
            // 
            this.player4.BackColor = System.Drawing.Color.Black;
            this.player4.Location = new System.Drawing.Point(569, 598);
            this.player4.Name = "player4";
            this.player4.Size = new System.Drawing.Size(23, 22);
            this.player4.Street = null;
            this.player4.TabIndex = 63;
            this.player4.Visible = false;
            // 
            // player3
            // 
            this.player3.BackColor = System.Drawing.Color.Yellow;
            this.player3.Location = new System.Drawing.Point(540, 598);
            this.player3.Name = "player3";
            this.player3.Size = new System.Drawing.Size(23, 22);
            this.player3.Street = null;
            this.player3.TabIndex = 62;
            this.player3.Visible = false;
            // 
            // player2
            // 
            this.player2.BackColor = System.Drawing.Color.Blue;
            this.player2.Location = new System.Drawing.Point(511, 598);
            this.player2.Name = "player2";
            this.player2.Size = new System.Drawing.Size(23, 22);
            this.player2.Street = null;
            this.player2.TabIndex = 61;
            this.player2.Visible = false;
            // 
            // player1
            // 
            this.player1.BackColor = System.Drawing.Color.Red;
            this.player1.Location = new System.Drawing.Point(483, 598);
            this.player1.Name = "player1";
            this.player1.Size = new System.Drawing.Size(23, 22);
            this.player1.Street = null;
            this.player1.TabIndex = 60;
            this.player1.Visible = false;
            // 
            // schlossstreet
            // 
            this.schlossstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.schlossstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.schlossstreet.Buyprice = 0;
            this.schlossstreet.Id = 0;
            this.schlossstreet.Location = new System.Drawing.Point(635, 610);
            this.schlossstreet.Name = "schlossstreet";
            this.schlossstreet.Next = null;
            this.schlossstreet.Size = new System.Drawing.Size(80, 60);
            this.schlossstreet.Streetname = "...";
            this.schlossstreet.TabIndex = 56;
            // 
            // addtax
            // 
            this.addtax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.addtax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.addtax.Buyprice = 0;
            this.addtax.Id = 0;
            this.addtax.Location = new System.Drawing.Point(635, 550);
            this.addtax.Name = "addtax";
            this.addtax.Next = null;
            this.addtax.Size = new System.Drawing.Size(80, 60);
            this.addtax.Streetname = "...";
            this.addtax.TabIndex = 55;
            // 
            // berlinerstreet
            // 
            this.berlinerstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.berlinerstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.berlinerstreet.Buyprice = 0;
            this.berlinerstreet.Id = 0;
            this.berlinerstreet.Location = new System.Drawing.Point(15, 130);
            this.berlinerstreet.Name = "berlinerstreet";
            this.berlinerstreet.Next = null;
            this.berlinerstreet.Size = new System.Drawing.Size(80, 60);
            this.berlinerstreet.Streetname = "...";
            this.berlinerstreet.TabIndex = 54;
            // 
            // wienerstreet
            // 
            this.wienerstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.wienerstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wienerstreet.Buyprice = 0;
            this.wienerstreet.Id = 0;
            this.wienerstreet.Location = new System.Drawing.Point(15, 190);
            this.wienerstreet.Name = "wienerstreet";
            this.wienerstreet.Next = null;
            this.wienerstreet.Size = new System.Drawing.Size(80, 60);
            this.wienerstreet.Streetname = "...";
            this.wienerstreet.TabIndex = 53;
            // 
            // parkstreet
            // 
            this.parkstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.parkstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.parkstreet.Buyprice = 0;
            this.parkstreet.Id = 0;
            this.parkstreet.Location = new System.Drawing.Point(635, 490);
            this.parkstreet.Name = "parkstreet";
            this.parkstreet.Next = null;
            this.parkstreet.Size = new System.Drawing.Size(80, 60);
            this.parkstreet.Streetname = "...";
            this.parkstreet.TabIndex = 35;
            // 
            // event3
            // 
            this.event3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.event3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.event3.Buyprice = 0;
            this.event3.Id = 0;
            this.event3.Location = new System.Drawing.Point(635, 430);
            this.event3.Name = "event3";
            this.event3.Next = null;
            this.event3.Size = new System.Drawing.Size(80, 60);
            this.event3.Streetname = "...";
            this.event3.TabIndex = 34;
            // 
            // hauptbhf
            // 
            this.hauptbhf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.hauptbhf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hauptbhf.Buyprice = 0;
            this.hauptbhf.Id = 0;
            this.hauptbhf.Location = new System.Drawing.Point(635, 370);
            this.hauptbhf.Name = "hauptbhf";
            this.hauptbhf.Next = null;
            this.hauptbhf.Size = new System.Drawing.Size(80, 60);
            this.hauptbhf.Streetname = "...";
            this.hauptbhf.TabIndex = 33;
            // 
            // bahnhofstreet
            // 
            this.bahnhofstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bahnhofstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bahnhofstreet.Buyprice = 0;
            this.bahnhofstreet.Id = 0;
            this.bahnhofstreet.Location = new System.Drawing.Point(635, 310);
            this.bahnhofstreet.Name = "bahnhofstreet";
            this.bahnhofstreet.Next = null;
            this.bahnhofstreet.Size = new System.Drawing.Size(80, 60);
            this.bahnhofstreet.Streetname = "...";
            this.bahnhofstreet.TabIndex = 32;
            // 
            // community3
            // 
            this.community3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.community3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.community3.Buyprice = 0;
            this.community3.Id = 0;
            this.community3.Location = new System.Drawing.Point(635, 250);
            this.community3.Name = "community3";
            this.community3.Next = null;
            this.community3.Size = new System.Drawing.Size(80, 60);
            this.community3.Streetname = "...";
            this.community3.TabIndex = 31;
            // 
            // hauptstreet
            // 
            this.hauptstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.hauptstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hauptstreet.Buyprice = 0;
            this.hauptstreet.Id = 0;
            this.hauptstreet.Location = new System.Drawing.Point(635, 190);
            this.hauptstreet.Name = "hauptstreet";
            this.hauptstreet.Next = null;
            this.hauptstreet.Size = new System.Drawing.Size(80, 60);
            this.hauptstreet.Streetname = "...";
            this.hauptstreet.TabIndex = 30;
            // 
            // rathausstreet
            // 
            this.rathausstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rathausstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rathausstreet.Buyprice = 0;
            this.rathausstreet.Id = 0;
            this.rathausstreet.Location = new System.Drawing.Point(635, 130);
            this.rathausstreet.Name = "rathausstreet";
            this.rathausstreet.Next = null;
            this.rathausstreet.Size = new System.Drawing.Size(80, 60);
            this.rathausstreet.Streetname = "...";
            this.rathausstreet.TabIndex = 29;
            // 
            // community2
            // 
            this.community2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.community2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.community2.Buyprice = 0;
            this.community2.Id = 0;
            this.community2.Location = new System.Drawing.Point(15, 250);
            this.community2.Name = "community2";
            this.community2.Next = null;
            this.community2.Size = new System.Drawing.Size(80, 60);
            this.community2.Streetname = "...";
            this.community2.TabIndex = 28;
            // 
            // muenchenerstreet
            // 
            this.muenchenerstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.muenchenerstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.muenchenerstreet.Buyprice = 0;
            this.muenchenerstreet.Id = 0;
            this.muenchenerstreet.Location = new System.Drawing.Point(15, 310);
            this.muenchenerstreet.Name = "muenchenerstreet";
            this.muenchenerstreet.Next = null;
            this.muenchenerstreet.Size = new System.Drawing.Size(80, 60);
            this.muenchenerstreet.Streetname = "...";
            this.muenchenerstreet.TabIndex = 27;
            // 
            // westbhf
            // 
            this.westbhf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.westbhf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.westbhf.Buyprice = 0;
            this.westbhf.Id = 0;
            this.westbhf.Location = new System.Drawing.Point(15, 370);
            this.westbhf.Name = "westbhf";
            this.westbhf.Next = null;
            this.westbhf.Size = new System.Drawing.Size(80, 60);
            this.westbhf.Streetname = "...";
            this.westbhf.TabIndex = 26;
            // 
            // neuestreet
            // 
            this.neuestreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.neuestreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.neuestreet.Buyprice = 0;
            this.neuestreet.Id = 0;
            this.neuestreet.Location = new System.Drawing.Point(15, 430);
            this.neuestreet.Name = "neuestreet";
            this.neuestreet.Next = null;
            this.neuestreet.Size = new System.Drawing.Size(80, 60);
            this.neuestreet.Streetname = "...";
            this.neuestreet.TabIndex = 25;
            // 
            // harborstreet
            // 
            this.harborstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.harborstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.harborstreet.Buyprice = 0;
            this.harborstreet.Id = 0;
            this.harborstreet.Location = new System.Drawing.Point(15, 490);
            this.harborstreet.Name = "harborstreet";
            this.harborstreet.Next = null;
            this.harborstreet.Size = new System.Drawing.Size(80, 60);
            this.harborstreet.Streetname = "...";
            this.harborstreet.TabIndex = 24;
            // 
            // electricity
            // 
            this.electricity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.electricity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.electricity.Buyprice = 0;
            this.electricity.Id = 0;
            this.electricity.Location = new System.Drawing.Point(15, 550);
            this.electricity.Name = "electricity";
            this.electricity.Next = null;
            this.electricity.Size = new System.Drawing.Size(80, 60);
            this.electricity.Streetname = "...";
            this.electricity.TabIndex = 23;
            // 
            // seestreet
            // 
            this.seestreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.seestreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.seestreet.Buyprice = 0;
            this.seestreet.Id = 0;
            this.seestreet.Location = new System.Drawing.Point(15, 610);
            this.seestreet.Name = "seestreet";
            this.seestreet.Next = null;
            this.seestreet.Size = new System.Drawing.Size(80, 60);
            this.seestreet.Streetname = "...";
            this.seestreet.TabIndex = 22;
            // 
            // goethestreet
            // 
            this.goethestreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.goethestreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.goethestreet.Buyprice = 0;
            this.goethestreet.Id = 0;
            this.goethestreet.Location = new System.Drawing.Point(575, 50);
            this.goethestreet.Name = "goethestreet";
            this.goethestreet.Next = null;
            this.goethestreet.Size = new System.Drawing.Size(60, 80);
            this.goethestreet.Streetname = "...";
            this.goethestreet.TabIndex = 21;
            // 
            // water
            // 
            this.water.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.water.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.water.Buyprice = 0;
            this.water.Id = 0;
            this.water.Location = new System.Drawing.Point(515, 50);
            this.water.Name = "water";
            this.water.Next = null;
            this.water.Size = new System.Drawing.Size(60, 80);
            this.water.Streetname = "...";
            this.water.TabIndex = 20;
            // 
            // schillerstreet
            // 
            this.schillerstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.schillerstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.schillerstreet.Buyprice = 0;
            this.schillerstreet.Id = 0;
            this.schillerstreet.Location = new System.Drawing.Point(455, 50);
            this.schillerstreet.Name = "schillerstreet";
            this.schillerstreet.Next = null;
            this.schillerstreet.Size = new System.Drawing.Size(60, 80);
            this.schillerstreet.Streetname = "...";
            this.schillerstreet.TabIndex = 19;
            // 
            // lessingstreet
            // 
            this.lessingstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lessingstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lessingstreet.Buyprice = 0;
            this.lessingstreet.Id = 0;
            this.lessingstreet.Location = new System.Drawing.Point(395, 50);
            this.lessingstreet.Name = "lessingstreet";
            this.lessingstreet.Next = null;
            this.lessingstreet.Size = new System.Drawing.Size(60, 80);
            this.lessingstreet.Streetname = "...";
            this.lessingstreet.TabIndex = 18;
            // 
            // northbhf
            // 
            this.northbhf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.northbhf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.northbhf.Buyprice = 0;
            this.northbhf.Id = 0;
            this.northbhf.Location = new System.Drawing.Point(335, 50);
            this.northbhf.Name = "northbhf";
            this.northbhf.Next = null;
            this.northbhf.Size = new System.Drawing.Size(60, 80);
            this.northbhf.Streetname = "...";
            this.northbhf.TabIndex = 17;
            // 
            // opernstreet
            // 
            this.opernstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.opernstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.opernstreet.Buyprice = 0;
            this.opernstreet.Id = 0;
            this.opernstreet.Location = new System.Drawing.Point(275, 50);
            this.opernstreet.Name = "opernstreet";
            this.opernstreet.Next = null;
            this.opernstreet.Size = new System.Drawing.Size(60, 80);
            this.opernstreet.Streetname = "...";
            this.opernstreet.TabIndex = 16;
            // 
            // museumstreet
            // 
            this.museumstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.museumstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.museumstreet.Buyprice = 0;
            this.museumstreet.Id = 0;
            this.museumstreet.Location = new System.Drawing.Point(215, 50);
            this.museumstreet.Name = "museumstreet";
            this.museumstreet.Next = null;
            this.museumstreet.Size = new System.Drawing.Size(60, 80);
            this.museumstreet.Streetname = "...";
            this.museumstreet.TabIndex = 15;
            // 
            // event2
            // 
            this.event2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.event2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.event2.Buyprice = 0;
            this.event2.Id = 0;
            this.event2.Location = new System.Drawing.Point(155, 50);
            this.event2.Name = "event2";
            this.event2.Next = null;
            this.event2.Size = new System.Drawing.Size(60, 80);
            this.event2.Streetname = "...";
            this.event2.TabIndex = 14;
            // 
            // theaterstreet
            // 
            this.theaterstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.theaterstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.theaterstreet.Buyprice = 0;
            this.theaterstreet.Id = 0;
            this.theaterstreet.Location = new System.Drawing.Point(95, 50);
            this.theaterstreet.Name = "theaterstreet";
            this.theaterstreet.Next = null;
            this.theaterstreet.Size = new System.Drawing.Size(60, 80);
            this.theaterstreet.Streetname = "...";
            this.theaterstreet.TabIndex = 13;
            // 
            // gotojail
            // 
            this.gotojail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.gotojail.BackgroundImage = global::Monopoly.Properties.Resources.monopoly_trauminsel_eckfelder_gehe_in_das_gefaengnis;
            this.gotojail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gotojail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.gotojail.Buyprice = 0;
            this.gotojail.Id = 0;
            this.gotojail.Location = new System.Drawing.Point(635, 50);
            this.gotojail.Name = "gotojail";
            this.gotojail.Next = null;
            this.gotojail.Size = new System.Drawing.Size(80, 80);
            this.gotojail.Streetname = "...";
            this.gotojail.TabIndex = 12;
            // 
            // freepark
            // 
            this.freepark.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.freepark.BackgroundImage = global::Monopoly.Properties.Resources.monopoly_eckfelder_free_parking_frei_parken;
            this.freepark.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.freepark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.freepark.Buyprice = 0;
            this.freepark.Id = 0;
            this.freepark.Location = new System.Drawing.Point(15, 50);
            this.freepark.Name = "freepark";
            this.freepark.Next = null;
            this.freepark.Size = new System.Drawing.Size(80, 80);
            this.freepark.Streetname = "...";
            this.freepark.TabIndex = 11;
            // 
            // jail
            // 
            this.jail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.jail.BackgroundImage = global::Monopoly.Properties.Resources.monopoly_eckfelder_jail_gefaengnis;
            this.jail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.jail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.jail.Buyprice = 0;
            this.jail.Id = 0;
            this.jail.Location = new System.Drawing.Point(15, 670);
            this.jail.Name = "jail";
            this.jail.Next = null;
            this.jail.Size = new System.Drawing.Size(80, 80);
            this.jail.Streetname = "...";
            this.jail.TabIndex = 10;
            // 
            // poststreet
            // 
            this.poststreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.poststreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.poststreet.Buyprice = 0;
            this.poststreet.Id = 0;
            this.poststreet.Location = new System.Drawing.Point(95, 670);
            this.poststreet.Name = "poststreet";
            this.poststreet.Next = null;
            this.poststreet.Size = new System.Drawing.Size(60, 80);
            this.poststreet.Streetname = "...";
            this.poststreet.TabIndex = 9;
            // 
            // elisenstreet
            // 
            this.elisenstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.elisenstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.elisenstreet.Buyprice = 0;
            this.elisenstreet.Id = 0;
            this.elisenstreet.Location = new System.Drawing.Point(155, 670);
            this.elisenstreet.Name = "elisenstreet";
            this.elisenstreet.Next = null;
            this.elisenstreet.Size = new System.Drawing.Size(60, 80);
            this.elisenstreet.Streetname = "...";
            this.elisenstreet.TabIndex = 8;
            // 
            // event1
            // 
            this.event1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.event1.BackgroundImage = global::Monopoly.Properties.Resources.monopoly_chance_ereignisfeld;
            this.event1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.event1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.event1.Buyprice = 0;
            this.event1.Id = 0;
            this.event1.Location = new System.Drawing.Point(215, 670);
            this.event1.Name = "event1";
            this.event1.Next = null;
            this.event1.Size = new System.Drawing.Size(60, 80);
            this.event1.Streetname = "...";
            this.event1.TabIndex = 7;
            // 
            // chausseestreet
            // 
            this.chausseestreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.chausseestreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chausseestreet.Buyprice = 0;
            this.chausseestreet.Id = 0;
            this.chausseestreet.Location = new System.Drawing.Point(275, 670);
            this.chausseestreet.Name = "chausseestreet";
            this.chausseestreet.Next = null;
            this.chausseestreet.Size = new System.Drawing.Size(60, 80);
            this.chausseestreet.Streetname = "...";
            this.chausseestreet.TabIndex = 6;
            // 
            // southbhf
            // 
            this.southbhf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.southbhf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.southbhf.Buyprice = 0;
            this.southbhf.Id = 0;
            this.southbhf.Location = new System.Drawing.Point(335, 670);
            this.southbhf.Name = "southbhf";
            this.southbhf.Next = null;
            this.southbhf.Size = new System.Drawing.Size(60, 80);
            this.southbhf.Streetname = "...";
            this.southbhf.TabIndex = 5;
            // 
            // incometax
            // 
            this.incometax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.incometax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.incometax.Buyprice = 0;
            this.incometax.Id = 0;
            this.incometax.Location = new System.Drawing.Point(395, 670);
            this.incometax.Name = "incometax";
            this.incometax.Next = null;
            this.incometax.Size = new System.Drawing.Size(60, 80);
            this.incometax.Streetname = "...";
            this.incometax.TabIndex = 4;
            // 
            // turmstreet
            // 
            this.turmstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.turmstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.turmstreet.Buyprice = 0;
            this.turmstreet.Id = 0;
            this.turmstreet.Location = new System.Drawing.Point(455, 670);
            this.turmstreet.Name = "turmstreet";
            this.turmstreet.Next = null;
            this.turmstreet.Size = new System.Drawing.Size(60, 80);
            this.turmstreet.Streetname = "...";
            this.turmstreet.TabIndex = 3;
            // 
            // community1
            // 
            this.community1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.community1.BackgroundImage = global::Monopoly.Properties.Resources.monopoly_community_chest_gemeinschaftsfeld;
            this.community1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.community1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.community1.Buyprice = 0;
            this.community1.Id = 0;
            this.community1.Location = new System.Drawing.Point(515, 670);
            this.community1.Name = "community1";
            this.community1.Next = null;
            this.community1.Size = new System.Drawing.Size(60, 80);
            this.community1.Streetname = "...";
            this.community1.TabIndex = 2;
            // 
            // badstreet
            // 
            this.badstreet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.badstreet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.badstreet.Buyprice = 0;
            this.badstreet.Id = 0;
            this.badstreet.Location = new System.Drawing.Point(575, 670);
            this.badstreet.Margin = new System.Windows.Forms.Padding(1);
            this.badstreet.Name = "badstreet";
            this.badstreet.Next = null;
            this.badstreet.Size = new System.Drawing.Size(60, 80);
            this.badstreet.Streetname = "...";
            this.badstreet.TabIndex = 1;
            // 
            // start
            // 
            this.start.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.start.BackgroundImage = global::Monopoly.Properties.Resources.monopoly_eckfelder_go_los;
            this.start.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.start.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.start.Buyprice = 0;
            this.start.Id = 0;
            this.start.Location = new System.Drawing.Point(635, 670);
            this.start.Margin = new System.Windows.Forms.Padding(1);
            this.start.Name = "start";
            this.start.Next = null;
            this.start.Size = new System.Drawing.Size(80, 80);
            this.start.Streetname = "...";
            this.start.TabIndex = 0;
            // 
            // streetname
            // 
            this.streetname.AutoSize = true;
            this.streetname.Location = new System.Drawing.Point(311, 490);
            this.streetname.Name = "streetname";
            this.streetname.Size = new System.Drawing.Size(0, 13);
            this.streetname.TabIndex = 73;
            // 
            // Gameboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(729, 753);
            this.Controls.Add(this.streetname);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.player8);
            this.Controls.Add(this.player7);
            this.Controls.Add(this.player6);
            this.Controls.Add(this.player5);
            this.Controls.Add(this.player4);
            this.Controls.Add(this.player3);
            this.Controls.Add(this.player2);
            this.Controls.Add(this.player1);
            this.Controls.Add(this.btn_roll);
            this.Controls.Add(this.dice2);
            this.Controls.Add(this.dice1);
            this.Controls.Add(this.schlossstreet);
            this.Controls.Add(this.addtax);
            this.Controls.Add(this.berlinerstreet);
            this.Controls.Add(this.wienerstreet);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.parkstreet);
            this.Controls.Add(this.event3);
            this.Controls.Add(this.hauptbhf);
            this.Controls.Add(this.bahnhofstreet);
            this.Controls.Add(this.community3);
            this.Controls.Add(this.hauptstreet);
            this.Controls.Add(this.rathausstreet);
            this.Controls.Add(this.community2);
            this.Controls.Add(this.muenchenerstreet);
            this.Controls.Add(this.westbhf);
            this.Controls.Add(this.neuestreet);
            this.Controls.Add(this.harborstreet);
            this.Controls.Add(this.electricity);
            this.Controls.Add(this.seestreet);
            this.Controls.Add(this.goethestreet);
            this.Controls.Add(this.water);
            this.Controls.Add(this.schillerstreet);
            this.Controls.Add(this.lessingstreet);
            this.Controls.Add(this.northbhf);
            this.Controls.Add(this.opernstreet);
            this.Controls.Add(this.museumstreet);
            this.Controls.Add(this.event2);
            this.Controls.Add(this.theaterstreet);
            this.Controls.Add(this.gotojail);
            this.Controls.Add(this.freepark);
            this.Controls.Add(this.jail);
            this.Controls.Add(this.poststreet);
            this.Controls.Add(this.elisenstreet);
            this.Controls.Add(this.event1);
            this.Controls.Add(this.chausseestreet);
            this.Controls.Add(this.southbhf);
            this.Controls.Add(this.incometax);
            this.Controls.Add(this.turmstreet);
            this.Controls.Add(this.community1);
            this.Controls.Add(this.badstreet);
            this.Controls.Add(this.start);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Gameboard";
            this.Text = "Monopoly";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Model.Street start;
        private Model.Street badstreet;
        private Model.Street community1;
        private Model.Street turmstreet;
        private Model.Street incometax;
        private Model.Street southbhf;
        private Model.Street chausseestreet;
        private Model.Street event1;
        private Model.Street elisenstreet;
        private Model.Street poststreet;
        private Model.Street jail;
        private Model.Street freepark;
        private Model.Street gotojail;
        private Model.Street theaterstreet;
        private Model.Street event2;
        private Model.Street museumstreet;
        private Model.Street opernstreet;
        private Model.Street northbhf;
        private Model.Street lessingstreet;
        private Model.Street schillerstreet;
        private Model.Street water;
        private Model.Street goethestreet;
        private Model.Street seestreet;
        private Model.Street electricity;
        private Model.Street harborstreet;
        private Model.Street neuestreet;
        private Model.Street westbhf;
        private Model.Street muenchenerstreet;
        private Model.Street community2;
        private Model.Street rathausstreet;
        private Model.Street hauptstreet;
        private Model.Street community3;
        private Model.Street bahnhofstreet;
        private Model.Street hauptbhf;
        private Model.Street event3;
        private Model.Street parkstreet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem spielToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionenToolStripMenuItem1;
        private Model.Street wienerstreet;
        private Model.Street berlinerstreet;
        private Model.Street addtax;
        private Model.Street schlossstreet;
        private System.Windows.Forms.Label dice1;
        private System.Windows.Forms.Label dice2;
        private System.Windows.Forms.Button btn_roll;
        private Model.Player player1;
        private Model.Player player2;
        private Model.Player player3;
        private Model.Player player4;
        private Model.Player player5;
        private Model.Player player6;
        private Model.Player player7;
        private Model.Player player8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label streetname;

    }
}

