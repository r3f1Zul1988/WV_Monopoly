﻿namespace Monopoly.Model
{
    partial class Street
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.price = new System.Windows.Forms.Label();
            this.streetname = new System.Windows.Forms.Label();
            this.streettype = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // price
            // 
            this.price.AutoSize = true;
            this.price.Location = new System.Drawing.Point(3, 45);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(19, 13);
            this.price.TabIndex = 1;
            this.price.Text = "0€";
            // 
            // streetname
            // 
            this.streetname.AutoSize = true;
            this.streetname.Location = new System.Drawing.Point(3, 10);
            this.streetname.Name = "streetname";
            this.streetname.Size = new System.Drawing.Size(25, 13);
            this.streetname.TabIndex = 2;
            this.streetname.Text = ".....-";
            // 
            // streettype
            // 
            this.streettype.AutoSize = true;
            this.streettype.Location = new System.Drawing.Point(3, 23);
            this.streettype.Name = "streettype";
            this.streettype.Size = new System.Drawing.Size(42, 13);
            this.streettype.TabIndex = 3;
            this.streettype.Text = "Strasse";
            // 
            // Street
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.streettype);
            this.Controls.Add(this.streetname);
            this.Controls.Add(this.price);
            this.Name = "Street";
            this.Size = new System.Drawing.Size(78, 78);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label price;
        private System.Windows.Forms.Label streetname;
        private System.Windows.Forms.Label streettype;
    }
}
