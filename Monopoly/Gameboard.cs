﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Monopoly.Controller;
using Monopoly.Model;

namespace Monopoly
{
    public partial class Gameboard : Form
    {
        private List<Player> players = new List<Player>();
        private List<Street> streets = new List<Street>();
        public Gameboard()
        {
            InitializeComponent();

            /**
             * ID 
             **/

            this.start.Id = 0;
            this.badstreet.Id = 1;
            this.community1.Id = 2;
            this.turmstreet.Id = 3;
            this.incometax.Id = 4;
            this.southbhf.Id = 5;
            this.chausseestreet.Id = 6;
            this.event1.Id = 7;
            this.elisenstreet.Id = 8;
            this.poststreet.Id = 9;
            this.jail.Id = 10;
            this.seestreet.Id = 11;
            this.electricity.Id = 12;
            this.harborstreet.Id = 13;
            this.neuestreet.Id = 14;
            this.westbhf.Id = 15;
            this.muenchenerstreet.Id = 16;
            this.community2.Id = 17;
            this.wienerstreet.Id = 18;
            this.berlinerstreet.Id = 19;
            this.freepark.Id = 20;
            this.theaterstreet.Id = 21;
            this.event2.Id = 22;
            this.museumstreet.Id = 23;
            this.opernstreet.Id = 24;
            this.northbhf.Id = 25;
            this.lessingstreet.Id = 26;
            this.schillerstreet.Id = 27;
            this.water.Id = 28;
            this.goethestreet.Id = 29;
            this.gotojail.Id = 30;
            this.rathausstreet.Id = 31;
            this.hauptstreet.Id = 32;
            this.community3.Id = 33;
            this.bahnhofstreet.Id = 34;
            this.hauptbhf.Id = 35;
            this.event3.Id = 36;
            this.parkstreet.Id  =37;
            this.incometax.Id = 38;
            this.schlossstreet.Id  = 39;

            /**
             * Typen
             **/
            this.start.setType(0);
            this.community1.setType(2);
            this.community2.setType(2);
            this.community3.setType(2);
            this.event1.setType(3);
            this.event2.setType(3);
            this.event3.setType(3);
            this.jail.setType(4);
            this.gotojail.setType(5);
            this.freepark.setType(6);
            this.incometax.setType(7);
            this.addtax.setType(8);

            /**
             * Namen
             **/
            this.badstreet.Streetname = "Bad-";
            this.turmstreet.Streetname = "Turm-";
            this.southbhf.Streetname = "Südbahnhof";
            this.chausseestreet.Streetname = "Chaussee";
            this.elisenstreet.Streetname = "Elisen";
            this.poststreet.Streetname = "Post";
            this.seestreet.Streetname = "See";
            this.electricity.Streetname = "Elektrizitätswerk";
            this.harborstreet.Streetname = "Hafen-";
            this.neuestreet.Streetname = "Neue ";
            this.westbhf.Streetname = "Westbahnhof";
            this.muenchenerstreet.Streetname = "Münchener-";
            this.wienerstreet.Streetname = "Wiener-";
            this.berlinerstreet.Streetname = "Berliner-";
            this.theaterstreet.Streetname = "Theater-";
            this.museumstreet.Streetname = "Museum-";
            this.opernstreet.Streetname = "Opern-";
            this.northbhf.Streetname = "Nordbahnhof";
            this.lessingstreet.Streetname = "Lessing-";
            this.schillerstreet.Streetname = "Schiller-";
            this.water.Streetname = "Wasserwerk";
            this.goethestreet.Streetname = "Goethe-";
            this.rathausstreet.Streetname = "Rathaus-";
            this.hauptstreet.Streetname = "Haupt-";
            this.bahnhofstreet.Streetname = "Bahnhof-";
            this.hauptbhf.Streetname = "Hauptbahnhof";
            this.parkstreet.Streetname = "Park-";
            this.schlossstreet.Streetname = "Schloss-";

            /**
             * Preise
             **/
            this.badstreet.Buyprice = 1200;
            this.turmstreet.Buyprice = 1200;
            this.southbhf.Buyprice = 4000;
            this.chausseestreet.Buyprice = 2000;
            this.elisenstreet.Buyprice = 2000;
            this.poststreet.Buyprice = 2400;
            this.seestreet.Buyprice = 2800;
            this.electricity.Buyprice = 3000;
            this.harborstreet.Buyprice = 2800;
            this.neuestreet.Buyprice = 3200;
            this.westbhf.Buyprice = 4000;
            this.muenchenerstreet.Buyprice = 3600;
            this.wienerstreet.Buyprice = 3600;
            this.berlinerstreet.Buyprice = 4000;
            this.theaterstreet.Buyprice = 4400;
            this.museumstreet.Buyprice = 4400;
            this.opernstreet.Buyprice = 4800;
            this.northbhf.Buyprice = 4000;
            this.lessingstreet.Buyprice = 5200;
            this.schillerstreet.Buyprice = 5200;
            this.water.Buyprice = 3000;
            this.goethestreet.Buyprice = 5600;
            this.rathausstreet.Buyprice = 6000;
            this.hauptstreet.Buyprice = 6000;
            this.bahnhofstreet.Buyprice = 6400;
            this.hauptbhf.Buyprice = 4000;
            this.parkstreet.Buyprice = 7000;
            this.schlossstreet.Buyprice = 8000;

            /**
            * Typen, bitte nicht vor die Preise schieben!
            **/
            this.badstreet.setType(1);
            this.turmstreet.setType(1);
            this.southbhf.setType(1);
            this.chausseestreet.setType(1);
            this.elisenstreet.setType(1);
            this.poststreet.setType(1);
            this.seestreet.setType(1);
            this.electricity.setType(1);
            this.harborstreet.setType(1);
            this.neuestreet.setType(1);
            this.westbhf.setType(1);
            this.muenchenerstreet.setType(1);
            this.wienerstreet.setType(1);
            this.berlinerstreet.setType(1);
            this.theaterstreet.setType(1);
            this.museumstreet.setType(1);
            this.opernstreet.setType(1);
            this.northbhf.setType(1);
            this.lessingstreet.setType(1);
            this.schillerstreet.setType(1);
            this.water.setType(1);
            this.goethestreet.setType(1);
            this.rathausstreet.setType(1);
            this.hauptstreet.setType(1);
            this.bahnhofstreet.setType(1);
            this.hauptbhf.setType(1);
            this.parkstreet.setType(1);
            this.schlossstreet.setType(1);

            /**
             * Ringverkettete Liste
             * **/
            this.start.Next = this.badstreet;
            this.badstreet.Next = this.community1;
            this.community1.Next = this.turmstreet;
            this.turmstreet.Next = this.incometax;
            this.incometax.Next = this.southbhf;
            this.southbhf.Next = this.chausseestreet;
            this.chausseestreet.Next = this.event1;
            this.event1.Next = this.elisenstreet;
            this.elisenstreet.Next = this.poststreet;
            this.poststreet.Next = this.jail;
            this.jail.Next = this.seestreet;
            this.seestreet.Next = this.electricity;
            this.electricity.Next = this.harborstreet;
            this.harborstreet.Next = this.neuestreet;
            this.neuestreet.Next = this.westbhf;
            this.westbhf.Next = this.muenchenerstreet;
            this.muenchenerstreet.Next = this.community2;
            this.community2.Next = this.wienerstreet;
            this.wienerstreet.Next = this.berlinerstreet;
            this.berlinerstreet.Next = this.freepark;
            this.freepark.Next = this.theaterstreet;
            this.theaterstreet.Next = this.event2;
            this.event2.Next = this.museumstreet;
            this.museumstreet.Next = this.opernstreet;
            this.opernstreet.Next = this.northbhf;
            this.northbhf.Next = this.lessingstreet;
            this.lessingstreet.Next = this.schillerstreet;
            this.schillerstreet.Next = this.water;
            this.water.Next = this.goethestreet;
            this.goethestreet.Next = this.gotojail;
            this.gotojail.Next = this.rathausstreet;
            this.rathausstreet.Next = this.hauptstreet;
            this.hauptstreet.Next = this.community3;
            this.community3.Next = this.bahnhofstreet;
            this.bahnhofstreet.Next = this.hauptbhf;
            this.hauptbhf.Next = this.event3;
            this.event3.Next = this.parkstreet;
            this.parkstreet.Next = this.addtax;
            this.addtax.Next = this.schlossstreet;
            this.schlossstreet.Next = this.start;

            /**
             * Liste
             * **/
            streets.Add(this.start);
            streets.Add(this.badstreet);
            streets.Add(this.community1);
            streets.Add(this.turmstreet);
            streets.Add(this.incometax);
            streets.Add(this.southbhf);
            streets.Add(this.chausseestreet);
            streets.Add(this.event1);
            streets.Add(this.elisenstreet);
            streets.Add(this.poststreet);
            streets.Add(this.jail);
            streets.Add(this.seestreet);
            streets.Add(this.electricity);
            streets.Add(this.harborstreet);
            streets.Add(this.neuestreet);
            streets.Add(this.westbhf);
            streets.Add(this.muenchenerstreet);
            streets.Add(this.community2);
            streets.Add(this.wienerstreet);
            streets.Add(this.berlinerstreet);
            streets.Add(this.freepark);
            streets.Add(this.theaterstreet);
            streets.Add(this.event2);
            streets.Add(this.museumstreet);
            streets.Add(this.opernstreet);
            streets.Add(this.northbhf);
            streets.Add(this.lessingstreet);
            streets.Add(this.schillerstreet);
            streets.Add(this.water);
            streets.Add(this.goethestreet);
            streets.Add(this.gotojail);
            streets.Add(this.rathausstreet);
            streets.Add(this.hauptstreet);
            streets.Add(this.community3);
            streets.Add(this.bahnhofstreet);
            streets.Add(this.hauptbhf);
            streets.Add(this.event3);
            streets.Add(this.parkstreet);
            streets.Add(this.addtax);
            streets.Add(this.schlossstreet);
        }

        private void startenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            player1.Visible = true;
            player2.Visible = true;
            players.Add(player1);
            players.Add(player2);
            if (GlobalVariables.maxplayer > 2) { player3.Visible = true; players.Add(player3); }
            if (GlobalVariables.maxplayer > 3) { player4.Visible = true; players.Add(player4); }
            if (GlobalVariables.maxplayer > 4) { player5.Visible = true; players.Add(player5); }
            if (GlobalVariables.maxplayer > 5) { player6.Visible = true; players.Add(player6); }
            if (GlobalVariables.maxplayer > 6) { player7.Visible = true; players.Add(player7); }
            if (GlobalVariables.maxplayer > 7) { player8.Visible = true; players.Add(player8); }
            GameLogic.startGame(this);
            startenToolStripMenuItem.Enabled = false;
            btn_roll.Enabled = true;
        }

        private void beendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public List<Street> getStreets()
        {
            return streets;
        }

        public List<Player> getPlayers()
        {
            return players;
        }

        private void btn_roll_Click(object sender, EventArgs e)
        {
            GameLogic.dice();
        }

        public void setDice1Text(String text)
        {
            dice1.Text = text;
        }

        public void setDice2Text(String text)
        {
            dice2.Text = text;
        }

        public void setStreetText(String text)
        { 
            streetname.Text = text ;
        }
    }
}
