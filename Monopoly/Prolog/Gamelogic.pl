% Autor: %Kevin Schmiedel
% Datum: %03.06.2013
%
%Logik und Quarkkekse

:- dynamic currentplayer/1.
:- dynamic dice1/1.
:- dynamic dice2/1.
:- dynamic rollallowed/1.
:- dynamic moveallowed/1.
%Eine Strasse ist kaufbar, wenn sie noch keiner gekauft hat und ihr Wert gr��er als 0 ist.
%kaufbar(index der Stra�e).
buyable(Index) :-  street(Index,-1,_,X,null), X>0.
%W�rfeln & Bewegen erlaubt
rollallowed(false).
moveallowed(false).
% Spielerhandling
% Es gibt zwei W�rfel
dice1(6).
dice2(6).
% Maximale Spieleranzahl
maxplayer(8).
% Spieler der am Zug ist
currentplayer(0).
% Startkapital
startcapital(15000).
% Spieler initialisieren
editplayer(player(PIndex, SIndex, Money, OwnStreets, Prisoned, FreeCards)) :- retract(player(PIndex, _, _, _, _, _)), assert(player(PIndex, SIndex, Money, OwnStreets, Prisoned, FreeCards)).
initplayers:- startcapital(Money),  maxplayer(Max),
                        Max > 0, editplayer(player(0, 0, Money, [], false, 0)),
                        writeln('Spieler 1 spielt mit.'),
                        Max > 1, editplayer(player(1, 0, Money, [], false, 0)),
                        writeln('Spieler 2 spielt mit.'),
                        Max > 2, editplayer(player(2, 0, Money, [], false, 0)),
                        writeln('Spieler 3 spielt mit.'),
                        Max > 3, editplayer(player(3, 0, Money, [], false, 0)),
                        writeln('Spieler 4 spielt mit.'),
                        Max > 4, editplayer(player(4, 0, Money, [], false, 0)),
                        writeln('Spieler 5 spielt mit.'),
                        Max > 5, editplayer(player(5, 0, Money, [], false, 0)),
                        writeln('Spieler 6 spielt mit.'),
                        Max > 6, editplayer(player(6, 0, Money, [], false, 0)),
                        writeln('Spieler 7 spielt mit.'),
                        Max > 7, editplayer(player(7, 0, Money, [], false, 0)),
                        writeln('Spieler 8 spielt mit.').
                        


% Gibt den Index des n�chsten Spielers zur�ck
nextplayer(Z) :- currentplayer(X),maxplayer(Y),X<(Y-1),retract(currentplayer(_)), Z is X+1, assert(currentplayer(Z)),!.
nextplayer(Z) :- retract(currentplayer(_)), assert(currentplayer(0)), Z is 0.

% Kauft eine Strasse. Klappt nur, wenn die Strasse "buyable" ist und der Spieler gen�gend "Money" zur Verf�gung hat (Money>=Y)
buy(Index, PIndex) :- buyable(Index), street(Index,-1,X,Y,_),player(PIndex,Pos,Money,Liste,Prison,Free), Money>=Y, retract(street(Index,-1,X,Y,null))
,assert(street(Index,PIndex,X,Y,rent)),retract(player(PIndex,Pos,Money,Liste,Prison,Free)),assert(player(PIndex,Pos,Money,[Liste,Index],Prison,Free)).

%W�rfelt die W�rfel (random(X,Y,A) setzt A auf eine Zuf�llige Zahl zwischen X und Y, eingeschlossen X, ausgeschlossen Y!
roll(A,B) :- rollallowed(Allowed), Allowed = true, retract(dice1(_)), retract(dice2(_)),random(1,7,A),random(1,7,B), assert(dice1(A)),
assert(dice2(B)),retract(rollallowed(_)), assert(rollallowed(false)), retract(moveallowed(_)), assert(moveallowed(true)).

%Bewegt eine Figur zu einem Feld, ist nur m�glich, wenn moveallowed true ist, und auch nur f�r den aktuellen Spieler!
move(PIndex) :- currentplayer(PIndex), moveallowed(true), dice1(Dice1), dice2(Dice2), player(PIndex, SIndex, Money,OwnStreets, Prisoned, FreeCards),
NewSIndex is SIndex + Dice1 + Dice2, editplayer(player(PIndex, NewSIndex, Money, OwnStreets, Prisoned, FreeCards)),
NewSIndex >=40, NNewSIndex is NewSIndex - 40,editplayer(player(PIndex, NNewSIndex, Money, OwnStreets, Prisoned, FreeCards)),
retract(moveallowed(_)), assert(moveallowed(false)), street(NewSIndex,_,Streetname,_,_), write('Spieler '), write(PIndex), write(' bewegt sich zu Feld '), writeln(Streetname),!.

move(PIndex) :- currentplayer(PIndex), moveallowed(true), player(PIndex, SIndex, _,_, _, _), retract(moveallowed(_)), assert(moveallowed(false)), street(SIndex,_,Streetname,_,_), write('Spieler '), write(PIndex), write(' bewegt sich zu Feld '), writeln(Streetname),!.

%Startet das Spiel - Initialisiert alle teilnehmenden Spieler, erlaubt einmal das W�rfeln und schreibt Spiel gestartet!
startgame :- initplayers,fail.
startgame :- retract(rollallowed(_)), assert(rollallowed(true)),fail.
startgame :- writeln('Spiel gestartet, viel Spa�!').