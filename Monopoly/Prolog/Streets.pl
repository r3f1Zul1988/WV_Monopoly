% Autor: n317h
% Datum: 02.06.2013

% Straße street(index, spielerindex, titel, preis, effect)
:- dynamic street/5.
street(0, -1, 'Los', 0, 'Ziehen Sie im Vorübergehen EURO 4000,- Gehalt ein.').
street(1, -1, 'Badstrasse', 1200, null).
street(2, -1, 'Gemeinschaftsfeld', 0, 'Gemeinschaftskarte ziehen').
street(3, -1, 'Turmstrasse', 1200, null).
street(4, -1, 'Einkommensteuer', 0, '4000,- EURO zahlen').
street(5, -1, 'Südbahnhof', 4000, null).
street(6, -1, 'Chausseestrasse', 2000, null).
street(7, -1, 'Ereignisfeld', 0, 'Ereigniskarte ziehen').
street(8, -1, 'Elisenstrasse', 2000, null).
street(9, -1, 'Poststrasse', 2400, null).
street(10, -1, 'Nur zu Besuch', 0, null).
street(11, -1, 'Seestrasse', 2800, null).
street(12, -1, 'Elektrizitätswerk', 3000, null).
street(13, -1, 'Hafenstrasse', 2800, null).
street(14, -1, 'Neue Strasse', 3200, null).
street(15, -1, 'Westbahnhof', 4000, null).
street(16, -1, 'Münchener Strasse', 3600, null).
street(17, -1, 'Gemeinschaftsfeld', 0, 'Gemeinschaftskarte ziehen').
street(18, -1, 'Wiener Strasse', 3600, null).
street(19, -1, 'Berliner Strasse', 4000, null).
street(20, -1, 'Frei parken', 0, null).
street(21, -1, 'Theaterstrasse', 4400, null).
street(22, -1, 'Ereignisfeld', 0, 'Ereigniskarte ziehen').
street(23, -1, 'Museumstrasse', 4400, null).
street(24, -1, 'Opernplatz', 4800, null).
street(25, -1, 'Nordbahnhof', 4000, null).
street(26, -1, 'Lessingstrasse', 5200, null).
street(27, -1, 'Schillerstrasse', 5200, null).
street(28, -1, 'Wasserwerk', 3000, null).
street(29, -1, 'Goethestrasse', 5600, null).
street(30, -1, 'Gehen Sie in das Gefängnis', 0, 'Auf das Feld "Im Gefängnis" ziehen').
street(31, -1, 'Rathausplatz', 6000, null).
street(32, -1, 'Hauptstrasse', 6000, null).
street(33, -1, 'Gemeinschaftsfeld', 0, 'Gemeinschaftskarte ziehen').
street(34, -1, 'Bahhofstrasse', 6400, null).
street(35, -1, 'Hauptbahnhof', 4000, null).
street(36, -1, 'Ereignisfeld', 0, 'Ereigniskarte ziehen').
street(37, -1, 'Parkstrasse', 7000, null).
street(38, -1, 'Zusatzsteuer', 0, '2000,- EURO zahlen').
street(39, -1, 'Schlossallee', 8000, null).