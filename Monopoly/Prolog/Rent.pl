% Autor: n317h
% Datum: 03.06.2013

% rent(index, type, default, one, two, three, four, hotel)
rent(1, street, 40, 200, 600, 1800, 3200, 5000).
rent(3, street, 80, 400, 1200, 3600, 6400, 9000).
rent(5, station, 0, 500, 1000, 2000, 4000, 0).
rent(6, street, 120, 600, 1800, 5400, 8000, 11000).
rent(8, street, 120, 600, 1800, 5400, 8000, 11000).
rent(9, street, 160, 800, 2000, 6000, 9000, 12000).
rent(11, street, 200, 1000, 3000, 9000, 12500, 15000).
rent(12, factory, 0, 0, 0, 0, 0, 0).
rent(13, street, 200, 1000, 3000, 9000, 12500, 15000).
rent(14, street, 240, 1200, 3600, 10000, 14000, 18000).
rent(15, station, 0, 500, 1000, 2000, 4000, 0).
rent(16, street, 280, 1400, 4000, 11000, 15000, 19000).
rent(18, street, 280, 1400, 4000, 11000, 15000, 19000).
rent(19, street, 320, 1600, 4400, 12000, 16000, 20000).
rent(21, street, 360, 1800, 5000, 14000, 17500, 21000).
rent(23, street, 360, 1800, 5000, 14000, 17500, 21000).
rent(24, street, 400, 2000, 6000, 15000, 18500, 22000).
rent(25, station, 0, 500, 1000, 2000, 4000, 0).
rent(26, street, 440, 2200, 6600, 16000, 19500, 23000).
rent(27, street, 440, 2200, 6600, 16000, 19500, 23000).
rent(28, factory, 0, 0, 0, 0, 0, 0).
rent(29, street, 480, 2400, 7200, 17000, 20500, 24000).
rent(31, street, 520, 2600, 7800, 18000, 22000, 25500).
rent(32, street, 520, 2600, 7800, 18000, 22000, 25500).
rent(34, street, 560, 3000, 9000, 20000, 24000, 28000).
rent(35, station, 0, 500, 1000, 2000, 4000, 0).
rent(37, street, 700, 3500, 10000, 22000, 26000, 30000).
rent(39, street, 1000, 4000, 12000, 28000, 34000, 40000).
